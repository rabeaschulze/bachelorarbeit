class Institute < ApplicationRecord
    has_many :students, dependent: :destroy
end
