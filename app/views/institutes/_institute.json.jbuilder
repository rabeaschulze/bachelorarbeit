json.extract! institute, :id, :name, :number_of_professors, :number_of_employees, :excess_capacity, :created_at, :updated_at
json.url institute_url(institute, format: :json)
