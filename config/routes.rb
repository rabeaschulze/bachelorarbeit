Rails.application.routes.draw do
  resources :institutes
  resources :students
  root   'static_pages#home'
  get    '/help', to: 'static_pages#help'
  get    '/about', to: 'static_pages#about'
  get    '/contact', to: 'contacts#new'
  get    '/signup', to: 'users#new'
  get    '/login', to: 'sessions#new'
  get    'matching_start', to: 'static_pages#matching_start'
  post   '/login', to: 'sessions#create'
  delete '/logout', to: 'sessions#destroy'
  match '/contacts', to: 'contacts#new', via: 'get'
  resources :contacts, only: [:new, :create]
  resources :users
end
